package ru.pcs.web.services;

import ru.pcs.web.dto.SignInForm;
import ru.pcs.web.models.AccountCostumer;
import ru.pcs.web.repositories.CostumerRepository;

import java.util.Optional;

public class SignInServiceImpl implements SignInService{
    private final CostumerRepository costumerRepository;
    public SignInServiceImpl(CostumerRepository costumerRepository) {
        this.costumerRepository = costumerRepository;
    }

    @Override
    public boolean doAuthenticate(SignInForm form) {
        Optional<AccountCostumer> accountCostumerOptional = costumerRepository.findByEmail(form.getEmail());

        return accountCostumerOptional.map(accountCostumer -> accountCostumer.getPassword().equals(form.getPassword())).orElse(false);

    }
}
