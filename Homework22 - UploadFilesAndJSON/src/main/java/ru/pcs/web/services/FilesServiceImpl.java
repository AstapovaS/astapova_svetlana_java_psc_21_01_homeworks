package ru.pcs.web.services;

import ru.pcs.web.dto.AccountDTO;
import ru.pcs.web.dto.FileDto;
import ru.pcs.web.models.AccountCostumer;
import ru.pcs.web.models.FileInfo;
import ru.pcs.web.repositories.CostumerRepository;
import ru.pcs.web.repositories.FileInfoRepository;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


public class FilesServiceImpl implements FilesService {

    private final FileInfoRepository fileInfoRepository;
    private final CostumerRepository costumerRepository;
    private String storagePath;


    public FilesServiceImpl(FileInfoRepository fileInfoRepository, CostumerRepository costumerRepository) {
        this.fileInfoRepository = fileInfoRepository;
        this.costumerRepository = costumerRepository;
    }

    @Override
    public void upload(FileDto form) {
        String fileName = form.getFileName();
        String extension = fileName.substring(fileName.lastIndexOf("."));

        FileInfo fileInfo = FileInfo.builder()
                .originalFileName(fileName)
                .storageFileName(UUID.randomUUID() + extension)
                .description(form.getDescription())
                .mimeType(form.getMimeType())
                .size(form.getSize())
                .idAccountCostumer(form.getIdAccountDto()) //тут сохраняется id аккаунту
                .build();

        fileInfoRepository.save(fileInfo); //сохраняем информацию в репозитории

        if (storagePath != null) {
            try {
                Files.copy(form.getFileStream(), Paths.get(storagePath + fileInfo.getStorageFileName()));
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }

    @Override
    public void setStoragePath(String path) {
        this.storagePath = path;
    }

    @Override
    public FileDto getFile(String storageFileName) {
        Optional<FileInfo> fileInfoOptional = fileInfoRepository.findByStorageName(storageFileName);
        if (fileInfoOptional.isPresent()) {
            FileDto file = FileDto.from(fileInfoOptional.get());
            file.setFileName(storageFileName);
            return file;
        }
        return null;
    }

    @Override
    public void writeFile(FileDto file, OutputStream outputStream) {
        try {
            Files.copy(Paths.get(storagePath + "\\" + file.getFileName()), outputStream);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public AccountDTO getCurrentAccountByEmail(String email) {
        return AccountDTO.from(costumerRepository.findByEmail(email).get());
    }

    @Override
    public List<FileDto> searchByOriginalFileName(String originalFileName, Long userId) {
        return FileDto.from(fileInfoRepository.searchByOriginalFileName(originalFileName, userId));
    }

}
