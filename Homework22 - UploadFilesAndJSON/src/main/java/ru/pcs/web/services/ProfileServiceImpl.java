package ru.pcs.web.services;

import ru.pcs.web.dto.AccountDTO;
import ru.pcs.web.repositories.CostumerRepository;

public class ProfileServiceImpl implements ProfileService{
    private CostumerRepository repository;

    public ProfileServiceImpl(CostumerRepository repository) {
        this.repository = repository;
    }

    @Override
    public AccountDTO getAccountByEmail(String email) {
        return AccountDTO.from(repository.findByEmail(email).get());
    }
}
