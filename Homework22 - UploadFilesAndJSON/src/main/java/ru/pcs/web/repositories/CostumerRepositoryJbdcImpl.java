package ru.pcs.web.repositories;

import ru.pcs.web.models.AccountCostumer;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class CostumerRepositoryJbdcImpl implements CostumerRepository{

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from account_costumer order by id;";

    //language=SQL
    private static final String SQL_INSERT = "insert into account_costumer(name, email, password) values (?, ?, ?)";

    //language=SQL
    private static final String SQL_SELECT_BY_EMAIL = "select * from account_costumer where email = ?";

    //language=SQL
    private static final String SQL_LIKE_BY_EMAIL = "select * from account_costumer where email like ?";

    private final DataSource dataSource;

    public CostumerRepositoryJbdcImpl(DataSource dataSource){
        this.dataSource = dataSource;
    }

    private static final Function<ResultSet, AccountCostumer> costumerMapper = row -> {
        try {
            return AccountCostumer.builder()
                    .id(row.getLong("id"))
                    .name(row.getString("name"))
                    .email(row.getString("email"))
                    .password(row.getString("password"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    };


    @Override
    public void save(AccountCostumer costumer) {
        try (Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, costumer.getName());
            statement.setString(2, costumer.getEmail());
            statement.setString(3, costumer.getPassword());

            int affectedRow = statement.executeUpdate();

            if (affectedRow != 1) {
                throw new SQLException("Can`t insert account by costumer");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                costumer.setId(generatedKeys.getLong("id"));
            } else {
                throw new SQLException("Can`t get id");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public List<AccountCostumer> findAll() {
        List<AccountCostumer> costumers = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL)){
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    costumers.add(costumerMapper.apply(resultSet));
                }
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return costumers;
    }

    @Override
    public Optional<AccountCostumer> findByEmail(String email) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_EMAIL)){
            statement.setString(1, email);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(costumerMapper.apply(resultSet));
                }

            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public List<AccountCostumer> searchByEmail(String email) {
        List<AccountCostumer> costumers = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_LIKE_BY_EMAIL)){
            statement.setString(1, "%" + email + "%");
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    costumers.add(costumerMapper.apply(resultSet));
                }
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return costumers;
    }
}
