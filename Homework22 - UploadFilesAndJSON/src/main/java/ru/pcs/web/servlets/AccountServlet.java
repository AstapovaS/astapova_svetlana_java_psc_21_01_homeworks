package ru.pcs.web.servlets;


import ru.pcs.web.dto.AccountDTO;
import ru.pcs.web.repositories.CostumerRepository;
import ru.pcs.web.repositories.CostumerRepositoryJbdcImpl;
import ru.pcs.web.services.AccountsService;
import ru.pcs.web.services.AccountsServiceImpl;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;

@WebServlet("/accounts")
public class AccountServlet extends HttpServlet {

    private AccountsService accountsService;

    @Override
    public void init(ServletConfig config) throws ServletException {

        ServletContext servletContext = config.getServletContext();
        DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");

        CostumerRepository repository = new CostumerRepositoryJbdcImpl(dataSource);

        this.accountsService = new AccountsServiceImpl(repository);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<AccountDTO> accounts = accountsService.getAll();
        request.setAttribute("accounts", accounts);
        request.getRequestDispatcher("jsp/accounts.jsp").forward(request, response);
    }

}
