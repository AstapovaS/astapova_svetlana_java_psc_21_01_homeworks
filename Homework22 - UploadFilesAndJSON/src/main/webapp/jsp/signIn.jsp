<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign In</title>
</head>
<body>
<h1 style="color: ${color}">Sign In Page</h1>
<h2>Please enter your data:</h2>
<form action="/signIn" method="post" >
    <label for="email">Enter your email:</label>
    <input type="email" id="email" name="email" placeholder="Your email">
    <br>
    <label for="password">Enter your password:</label>
    <input type="password" id="password" name="password" placeholder="Your password">
    <br>
    <input type="submit" value="Sign In">
</form>

</body>
</html>