import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class ColorHandler extends CarHandler{

    @Override
    public void write(String[] carsArray) {
        try (BufferedWriter writer =
                     new BufferedWriter(new FileWriter("Homework13 - FileAnalysis\\/cars_color.txt", true))){
            writer.write(carsArray[2]);
            writer.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
