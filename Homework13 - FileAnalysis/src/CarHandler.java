public abstract class CarHandler {
    private CarHandler nextHandler;

    public void setNextHandler(CarHandler nextHandler) {
        this.nextHandler = nextHandler;
    }

    public void handler() {
        if(nextHandler!=null){
            nextHandler.handler();
        }
    }

    public abstract void write(String[] carsArray);
}
