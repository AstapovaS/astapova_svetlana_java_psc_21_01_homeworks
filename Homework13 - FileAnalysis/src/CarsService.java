import java.io.File;
import java.util.ArrayList;

public class CarsService {

    void toProcessListCars(String nameFileWithCars) {

        ArrayList<String> listCarsArrays;  // список всех автомобилей из файла

        long fileSize;

        File file = new File(nameFileWithCars);
        fileSize = file.length();
        ReaderFileWithCars readerFileWithCars1;

        if (fileSize > 2048) {
            readerFileWithCars1 = new ReaderFileWithCarsWithReadLine();
        } else {
            readerFileWithCars1 = new ReaderFileWithCarsWithReadLine();
        }


        // считываем все данные из файла в список
        listCarsArrays = readerFileWithCars1.read(nameFileWithCars);

        // запускаем обработку для каждого элемента списка

        for (String listCarsArray : listCarsArrays) {
            String[] carsArray = listCarsArray.split(" ");

            // проверяем номера автомобилей

            CarsObserver checkStolenCarsNumber = new CheckStolenCarsNumber();
            checkStolenCarsNumber.update(carsArray[0]);

            CarsObserver checkValidNumberCars = new CheckValidNumberCars();
            checkValidNumberCars.update(carsArray[0]);

            // записываем данные в разные файлы

            CarHandler numbers = new NumberHandler();
            CarHandler colors = new ColorHandler();
            CarHandler models = new ModelHandler();
            CarHandler mileage = new MileageHandler();
            CarHandler price = new PriceHandler();

            numbers.setNextHandler(colors);
            colors.setNextHandler(models);
            models.setNextHandler(mileage);
            mileage.setNextHandler(price);

            numbers.write(carsArray);
            colors.write(carsArray);
            models.write(carsArray);
            mileage.write(carsArray);
            price.write(carsArray);

        }
    }
}
