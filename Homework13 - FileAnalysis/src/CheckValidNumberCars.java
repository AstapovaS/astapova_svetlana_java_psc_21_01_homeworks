import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Pattern;

public class CheckValidNumberCars implements CarsObserver{
    @Override
    public void update(String carsNumber) {
        try (BufferedWriter writer = new BufferedWriter
                (new FileWriter("Homework13 - FileAnalysis\\/cars_blacklist.txt", true))) {
            String patternNumber = "[А-Яа-я][0-9]{3}[А-Яа-я]{2}";
            if (!Pattern.matches(patternNumber, carsNumber)) {
                System.out.println("Номер автомобиля " + carsNumber + " не соответствует формату и " +
                        "вносится в черный список");
               writer.write(carsNumber);
               writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
