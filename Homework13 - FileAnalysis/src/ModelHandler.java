import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class ModelHandler extends CarHandler{

    @Override
    public void write(String[] carsArray) {
        try (BufferedWriter writer =
                     new BufferedWriter(new FileWriter("Homework13 - FileAnalysis\\/cars_models.txt", true))){
            writer.write(carsArray[1]);
            writer.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
