import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.stream.Stream;

public class ReaderFileWithCarsWithStream implements ReaderFileWithCars{
    @Override
    public ArrayList<String> read(String nameFileWithCars) {
        ArrayList<String> listCarsArrays = new ArrayList<>();

        File file = new File(nameFileWithCars);
        try (Stream<String> lines = Files.lines(file.toPath()))
        {
            lines.forEach(listCarsArrays::add);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listCarsArrays;
    }
}
