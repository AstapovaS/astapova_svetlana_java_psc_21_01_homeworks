import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class PriceHandler extends CarHandler{
   @Override
    public void write(String[] carsArray) {
        try (BufferedWriter writer =
                     new BufferedWriter(new FileWriter("Homework13 - FileAnalysis\\/cars_price.txt", true))){
            writer.write(carsArray[4]);
            writer.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
