import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ReaderFileWithCarsWithReadLine implements ReaderFileWithCars{
    @Override
    public ArrayList<String> read(String nameFileWithCars) {
        ArrayList<String> listCarsArrays = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(nameFileWithCars))) {
            while (reader.ready()) {
                listCarsArrays.add(reader.readLine());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    return listCarsArrays;
    }
}
