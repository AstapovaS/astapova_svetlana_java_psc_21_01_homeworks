import java.util.ArrayList;

/**
 * Ридер возвращает аррайлист и вызывается всего один раз
 */

public interface ReaderFileWithCars {
    ArrayList<String> read(String nameFileWithCars);
}
