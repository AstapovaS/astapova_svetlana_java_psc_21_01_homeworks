import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CheckStolenCarsNumber implements CarsObserver{
    @Override
    public void update(String carsNumber) {
        try (BufferedReader reader = new BufferedReader
                (new FileReader("Homework13 - FileAnalysis\\/stolen_cars.txt"))){
            while (reader.ready()) {
                String stolenCarsNumber = reader.readLine();
                if (stolenCarsNumber.equals(carsNumber)) {
                    System.out.println("Машина под номером " + carsNumber + " числится в угоне!");
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
