create table account_costumer (
    id bigserial primary key,
    name varchar(70),
    email varchar(40),
    password varchar(40)
);

drop  table if exists  file_info;

create table file_info (
    id bigserial primary key,
    original_file_name varchar(100),
    storage_file_name varchar(100),
    size bigint,
    mime_type varchar(30),
    description varchar(100),
    current_user_id bigint,
    foreign key (current_user_id) references account_costumer (id)
);
