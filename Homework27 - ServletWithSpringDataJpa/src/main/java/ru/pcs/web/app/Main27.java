package ru.pcs.web.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.pcs.web.config.ApplicationConfig;
import ru.pcs.web.dto.SignUpForm;
import ru.pcs.web.models.AccountCostumer;
import ru.pcs.web.repositories.CostumerRepository;
import ru.pcs.web.repositories.FileInfoRepository;
import ru.pcs.web.services.*;

import java.util.Properties;

public class Main27 {
    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        SignUpService signUpService = context.getBean(SignUpService.class);

        CostumerRepository accountsRepository = context.getBean(CostumerRepository.class);
        FileInfoRepository fileInfoRepository = context.getBean(FileInfoRepository.class);
        FilesService filesService = context.getBean(FilesService.class);
        AccountCostumer costumer = accountsRepository.findAccountCostumerByEmail("morgan159@mail.ru").get();
        AccountCostumer costumer1 = accountsRepository.findAccountCostumerByEmail("yan@mail.ru").get();
        System.out.println("_________________" + filesService.searchByOriginalFileName("2D", costumer.getId()));




    }
}
