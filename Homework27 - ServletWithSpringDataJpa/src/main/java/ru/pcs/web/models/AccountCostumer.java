package ru.pcs.web.models;

import lombok.*;
import lombok.experimental.FieldNameConstants;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class AccountCostumer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String email;
    private String password;

    @OneToMany(mappedBy = "accountCostumer")
    @ToString.Exclude
    private List<FileInfo> files;
}
