package ru.pcs.web.models;

import lombok.*;
import org.springframework.stereotype.Component;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@ToString(exclude = "accountCostumer")
public class FileInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String originalFileName;
    private String storageFileName;
    private Long size;
    private String mimeType;
    private String description;

    @ManyToOne
    @JoinColumn(name = "current_user_id")
    private AccountCostumer accountCostumer;
}

