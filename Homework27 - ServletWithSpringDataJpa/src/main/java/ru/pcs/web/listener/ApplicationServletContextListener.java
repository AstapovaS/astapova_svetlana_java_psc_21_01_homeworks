package ru.pcs.web.listener;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.pcs.web.config.ApplicationConfig;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.Properties;

@WebListener
public class ApplicationServletContextListener implements ServletContextListener {

    private HikariDataSource dataSource;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();

        Properties properties = new Properties();
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream("application.properties"));
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        this.dataSource = context.getBean(HikariDataSource.class);

        servletContext.setAttribute("context", context);
        servletContext.setAttribute("storagePath", properties.getProperty("storage.path"));

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        dataSource.close();
    }
}
