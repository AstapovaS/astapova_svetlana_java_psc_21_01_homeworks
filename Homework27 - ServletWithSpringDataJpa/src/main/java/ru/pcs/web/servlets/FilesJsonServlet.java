package ru.pcs.web.servlets;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.pcs.web.config.ApplicationConfig;
import ru.pcs.web.dto.FileDto;
import ru.pcs.web.models.AccountCostumer;
import ru.pcs.web.services.AccountsService;
import ru.pcs.web.services.FilesService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/files/json")
public class FilesJsonServlet extends HttpServlet {
    private FilesService filesService;
    private ObjectMapper objectMapper;

    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("context");
        this.filesService = springContext.getBean(FilesService.class);

        this.objectMapper = new ObjectMapper();
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Long userId = filesService.getCurrentAccountByEmail(SignInServlet.emailOfCurrentAccount).getId();
        List<FileDto> files = filesService.searchByOriginalFileName(request.getParameter("originalFileName"), userId);
        response.setContentType("application/json");
        String json = objectMapper.writeValueAsString(files);
        response.getWriter().println(json);
    }

}
