package ru.pcs.web.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.pcs.web.config.ApplicationConfig;
import ru.pcs.web.dto.FileDto;
import ru.pcs.web.models.AccountCostumer;
import ru.pcs.web.services.AccountsService;
import ru.pcs.web.services.FilesService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.*;


@WebServlet("/filesUpload")
@MultipartConfig
public class FilesUploadServlet extends HttpServlet {

    private FilesService filesService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("context");
        this.filesService = springContext.getBean(FilesService.class);
        this.filesService.setStoragePath((String) servletContext.getAttribute("storagePath"));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/jsp/filesUpload.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader descriptionReader = new BufferedReader(
                new InputStreamReader(request.getPart("description").getInputStream()));

        Part filePart = request.getPart("file");
        // вытаскиваем текущего пользователя по email
        AccountCostumer accountCostumer = filesService.getCurrentAccountByEmail(SignInServlet.emailOfCurrentAccount);

        // создаем объект FileDTO,
        FileDto form = FileDto.builder()
                .description(descriptionReader.readLine())
                .fileName(filePart.getSubmittedFileName())
                .size(filePart.getSize())
                .mimeType(filePart.getContentType())
                .accountCostumer(accountCostumer) // в объекте сохраняется ID текущего пользователя
                .fileStream(filePart.getInputStream())
                .build();

        filesService.upload(form);

        response.sendRedirect("/files");
    }
}
