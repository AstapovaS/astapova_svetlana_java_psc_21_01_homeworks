package ru.pcs.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pcs.web.dto.SignUpForm;
import ru.pcs.web.models.AccountCostumer;
import ru.pcs.web.repositories.CostumerRepository;

import java.util.Locale;

@Service(value = "signUpService")
public class SignUpServiceImpl implements SignUpService{

    private final CostumerRepository costumerRepository;

    @Autowired
    public SignUpServiceImpl(CostumerRepository costumerRepository) {
        this.costumerRepository = costumerRepository;
    }

    @Override
    public void signUp(SignUpForm form) {
        AccountCostumer accountCostumer = AccountCostumer.builder()
                .name(form.getName())
                .email(form.getEmail().toLowerCase(Locale.ROOT))
                .password(form.getPassword())
                .build();
        costumerRepository.save(accountCostumer);
    }
}
