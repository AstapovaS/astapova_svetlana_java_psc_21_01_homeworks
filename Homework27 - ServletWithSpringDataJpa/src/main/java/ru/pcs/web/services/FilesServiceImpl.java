package ru.pcs.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.pcs.web.dto.FileDto;
import ru.pcs.web.models.AccountCostumer;
import ru.pcs.web.models.FileInfo;
import ru.pcs.web.repositories.CostumerRepository;
import ru.pcs.web.repositories.FileInfoRepository;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service(value = "FilesService")
public class FilesServiceImpl implements FilesService {

    private final FileInfoRepository fileInfoRepository;
    private final CostumerRepository costumerRepository;
    private String storagePath;

    @Autowired
    public FilesServiceImpl(FileInfoRepository fileInfoRepository, CostumerRepository costumerRepository) {
        this.fileInfoRepository = fileInfoRepository;
        this.costumerRepository = costumerRepository;
    }

    @Override
    public void upload(FileDto form) {
        String fileName = form.getFileName();
        String extension = fileName.substring(fileName.lastIndexOf("."));

        FileInfo fileInfo = FileInfo.builder()
                .originalFileName(fileName)
                .storageFileName(UUID.randomUUID() + extension)
                .description(form.getDescription())
                .mimeType(form.getMimeType())
                .size(form.getSize())
                .accountCostumer(form.getAccountCostumer()) //тут сохраняется id аккаунту
                .build();

        fileInfoRepository.save(fileInfo); //сохраняем информацию в репозитории

        if (storagePath != null) {
            try {
                Files.copy(form.getFileStream(), Paths.get(storagePath + fileInfo.getStorageFileName()));
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }

    @Override
    public void setStoragePath(String path) {
        this.storagePath = path;
    }

    @Override
    public FileDto getFile(String storageFileName) {
        Optional<FileInfo> fileInfoOptional = fileInfoRepository.findFileInfoByStorageFileName(storageFileName);
        if (fileInfoOptional.isPresent()) {
            FileDto file = FileDto.from(fileInfoOptional.get());
            file.setFileName(storageFileName);
            return file;
        }
        return null;
    }

    @Override
    public void writeFile(FileDto file, OutputStream outputStream) {
        try {
            Files.copy(Paths.get(storagePath + "\\" + file.getFileName()), outputStream);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public AccountCostumer getCurrentAccountByEmail(String email) {
        return costumerRepository.findAccountCostumerByEmail(email).get();
    }


    @Override
    public List<FileDto> searchByOriginalFileName(String originalFileName, Long accountCostumerId) {
        return FileDto.from(fileInfoRepository.findAllByOriginalFileNameLikeAndAccountCostumer_Id("%" + originalFileName +"%", accountCostumerId));
    }

}
