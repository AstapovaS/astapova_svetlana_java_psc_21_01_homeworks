package ru.pcs.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.pcs.web.dto.AccountDTO;
import static ru.pcs.web.dto.AccountDTO.from;
import ru.pcs.web.repositories.CostumerRepository;


@Service(value = "ProfileService")
public class ProfileServiceImpl implements ProfileService{
    private final CostumerRepository repository;

    @Autowired
    public ProfileServiceImpl(CostumerRepository repository) {
        this.repository = repository;
    }

    @Override
    public AccountDTO getAccountByEmail(String email) {
        return from(repository.findAccountCostumerByEmail(email).get());
    }
}
