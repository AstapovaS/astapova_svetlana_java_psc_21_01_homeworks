package ru.pcs.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.pcs.web.dto.SignInForm;
import ru.pcs.web.models.AccountCostumer;
import ru.pcs.web.repositories.CostumerRepository;

import java.util.Optional;

@Service(value = "SignInService")
public class SignInServiceImpl implements SignInService{
    private final CostumerRepository costumerRepository;

    @Autowired
    public SignInServiceImpl(CostumerRepository costumerRepository) {
        this.costumerRepository = costumerRepository;
    }

    @Override
    public boolean doAuthenticate(SignInForm form) {
        Optional<AccountCostumer> accountCostumerOptional = costumerRepository.findAccountCostumerByEmail(form.getEmail());

        return accountCostumerOptional.map(accountCostumer -> accountCostumer.getPassword().equals(form.getPassword())).orElse(false);

    }
}
