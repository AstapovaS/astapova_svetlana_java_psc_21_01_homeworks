package ru.pcs.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.pcs.web.dto.AccountDTO;
import ru.pcs.web.repositories.CostumerRepository;

import java.util.List;

import static ru.pcs.web.dto.AccountDTO.from;

@Service(value = "AccountService")
public class AccountsServiceImpl implements AccountsService{
    private final CostumerRepository repository;

    @Autowired
    public AccountsServiceImpl(CostumerRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<AccountDTO> getAll() {
        return from(repository.findAll());
    }

    @Override
    public List<AccountDTO> searchByEmail(String email) {
        return from(repository.findAccountCostumersByEmailContains(email));
    }


}
