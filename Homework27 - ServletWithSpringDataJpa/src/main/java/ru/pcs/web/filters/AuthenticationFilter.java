package ru.pcs.web.filters;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebFilter("/*")
@ComponentScan
public class AuthenticationFilter implements Filter {

    private static final List<String> PROTECTED_URIS = Arrays.asList(
            "/profile", "/accounts", "/searchUsers", "/filesUpload", "/files");

    public static final String DEFAULT_AUTHENTICATED_ATTRIBUTE_NAME = "isAuthenticated";

    private static final List<String> NOT_ACCESSED_AFTER_AUTHENTICATION_URIS = Arrays.asList(
            "singIn", "signUp");

    private static final String DEFAULT_REDIRECT_URI = "/profile";
    private static final String DEFAULT_SIGN_IN_URI = "/signIn";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        // защита
        if (isProtected(request)){
            // делаем проверку аутентификации
            if (isAuthenticated(request)) {
                filterChain.doFilter(request, response);
            } else {
                response.sendRedirect(DEFAULT_SIGN_IN_URI);
            }
            return;
        }
        if (isAuthenticated(request) && NOT_ACCESSED_AFTER_AUTHENTICATION_URIS.contains(request.getRequestURI())) {
            response.sendRedirect(DEFAULT_REDIRECT_URI);
            return;
        }

        filterChain.doFilter(request, response);
    }

    private boolean isAuthenticated(HttpServletRequest request){
        //запрашиваем сессию без создания
        HttpSession session = request.getSession(false);
        if (session == null) {
            return false;
        }

        Boolean result = (Boolean) session.getAttribute(DEFAULT_AUTHENTICATED_ATTRIBUTE_NAME);
        return result != null && result;

    }

    private boolean isProtected(HttpServletRequest request) {
        return PROTECTED_URIS.contains(request.getRequestURI());
    }

    @Override
    public void destroy() {

    }
}
