package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.AccountCostumer;

import java.util.List;
import java.util.Optional;

public interface CostumerRepository extends JpaRepository<AccountCostumer, Long> {
    Optional<AccountCostumer> findAccountCostumerByEmail(String email);
    List<AccountCostumer> findAccountCostumersByEmailContains(String email);
}
