package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.pcs.web.models.AccountCostumer;
import ru.pcs.web.models.FileInfo;

import java.util.List;
import java.util.Optional;


public interface FileInfoRepository extends JpaRepository<FileInfo, Long> {
    Optional<FileInfo> findFileInfoByStorageFileName(String storageFileName);
    List<FileInfo> findAllByOriginalFileNameLikeAndAccountCostumer_Id(String originalFileName, Long accountCostumer_id);
}
