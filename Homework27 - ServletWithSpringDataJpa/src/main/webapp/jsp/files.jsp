<%--<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Search Files
    </title>
</head>

<script>
    function searchFilesByUser(originalFileName) {
        let request = new XMLHttpRequest();
        request.open('GET', '/files/json?originalFileName=' + originalFileName, false);
        request.send();


                let html =
                    '<tr>' +
                    '<th>Original File Name</th>' +
                    '<th>Link</th>' +
                    '</tr>';

                let json = JSON.parse(request.response);
                for (let i = 0; i < json.length; i++) {
                    html += '<tr>'
                    html +=     '<td>' + json[i]['originalFileName'] + '</td>'
                    const url = "/file?fileName=" + json[i]['fileName'];
                    html += '<td><a href="' + url + '">' + json[i]['originalFileName'] + ' </a></td>'
                    html += '</tr>'
                }
                document.getElementById('files_table').innerHTML = html;

     }
</script>
<body>

<h1>Search your pictures</h1>


<label for="originalFileName">Enter name file: </label>
<input id="originalFileName" type="text" placeholder="Enter your text"
       onkeyup="searchFilesByUser(document.getElementById('originalFileName').value)">
<table id="files_table">

</table>

</body>
</html>
