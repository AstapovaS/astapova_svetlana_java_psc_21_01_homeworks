package ru.pcs.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Homework30SpringBootStartApplication {

    public static void main(String[] args) {
        SpringApplication.run(Homework30SpringBootStartApplication.class, args);
    }

}
