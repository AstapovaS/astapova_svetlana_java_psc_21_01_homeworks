package ru.pcs.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.web.dto.AddGoodForm;
import ru.pcs.web.models.Good;
import ru.pcs.web.repositories.GoodsRepository;

@Service
@RequiredArgsConstructor
public class AddGoodsServiceImpl implements AddGoodsService{

    private final GoodsRepository goodsRepository;

    @Override
    public void addGood(AddGoodForm form) {
        Good good = Good.builder()
                .goodName(form.getGoodName())
                .description(form.getDescription())
                .cost(form.getCost())
                .count(form.getCount())
                .build();

        goodsRepository.save(good);
    }
}
