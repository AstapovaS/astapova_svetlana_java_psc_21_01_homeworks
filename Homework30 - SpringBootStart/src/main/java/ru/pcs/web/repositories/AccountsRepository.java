package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.Account;


public interface AccountsRepository extends JpaRepository<Account, Long> {
}
