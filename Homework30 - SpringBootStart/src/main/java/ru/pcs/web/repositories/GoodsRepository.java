package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.Good;

public interface GoodsRepository extends JpaRepository<Good, Long> {
}


