create table costumer (
                          id serial primary key,
                          name text,
                          email text,
                          address text,
                          balance decimal
);

create table product (
                         id serial primary key,
                         name text,
                         category text,
                         price decimal,
                         stock integer,
                         discount integer
);

create table product_reservation (
                                     id serial primary key,
                                     product_id integer,
                                     foreign key (product_id) references product (id),
                                     costumer_id integer,
                                     foreign key (costumer_id) references costumer (id),
                                     count_product integer,
                                     date_product_reservation timestamp
);

create table order_products (
                                id serial primary key,
                                date_order timestamp,
                                product_reservation_id integer,
                                foreign key (product_reservation_id) references product_reservation (id)
);

create table order_reservation (
                                   id serial primary key,
                                   order_products_id integer,
                                   foreign key (order_products_id) references order_products (id),
                                   date_reservation timestamp
);

create table check_order (
                             id serial primary key,
                             order_reservation_id integer,
                             foreign key (order_reservation_id) references order_reservation (id),
                             date_check timestamp,
                             sum decimal
);