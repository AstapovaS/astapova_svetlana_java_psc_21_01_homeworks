import java.sql.*;

public class Main14 {
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "27091986";
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/test";

    public static void main(String[] args) {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
             Statement statement = connection.createStatement()) {

            /*
            Запрос выводит адреса и имена покупателей, заказавших сосны
             */

            String sql = "with costumer_order as (\n" +
                    "    select costumer_id\n" +
                    "    from product_reservation\n" +
                    "    where product_id = 1\n" +
                    ")\n" +
                    "select distinct address, name from costumer_order co\n" +
                    "                                       inner join costumer c on co.costumer_id = c.id;";
            try (ResultSet resultSet = statement.executeQuery(sql)) {
                while (resultSet.next()) {
                    System.out.println("Адрес покупателя: " +
                            resultSet.getString("address") +
                            ", имя покупателя " + resultSet.getString("name"));
                }
            }

            /*
                Запрос находит id, название и цену товара,
                чей остаток на складе больше 100,
                а количество заказанных единиц больше одного
             */

            String sql2 = "select p.id, p.name, p.price from product as p\n" +
                    "                                                      inner join (select pr.product_id " +
                    "from product_reservation as pr " +
                    "where pr.count_product > 1) " +
                    "product_count on p.id = product_count.product_id\n" +
                    "                where p.stock > 100;";

            try (ResultSet resultSet = statement.executeQuery(sql2)) {
                while (resultSet.next()) {
                    System.out.println("ID товара: " + resultSet.getInt("id") +
                            ", название товара:  " +
                            resultSet.getString("name") +
                            ", стоимость товара: "  +
                            resultSet.getDouble("price"));
                }
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}