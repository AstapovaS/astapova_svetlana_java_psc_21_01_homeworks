package ru.pcs.web.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.pcs.web.dto.SignUpForm;
import ru.pcs.web.models.AccountCostumer;
import ru.pcs.web.models.FileInfo;
import ru.pcs.web.repositories.*;
import ru.pcs.web.services.SignUpService;
import ru.pcs.web.services.SignUpServiceImpl;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Properties;

public class Main24 {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        EntityManager entityManager = sessionFactory.createEntityManager();
        CostumerRepository costumerRepository = new CostumerRepositoryJpa(entityManager);
        FileInfoRepository fileInfoRepository = new FileInfoRepositoryJpa(entityManager);

//        AccountCostumer accountCostumer = AccountCostumer.builder()
//                .name("Svetlana")
//                .email("www@mail.ru")
//                .password("27091986")
//                .build();
//
//        costumerRepository.save(accountCostumer);
//
//        FileInfo fileInfo = FileInfo.builder()
//                .originalFileName("Test1.jpg")
//                .storageFileName("1234567.jpg")
//                .accountCostumer(accountCostumer)
//                .build();
//
//        fileInfoRepository.save(fileInfo);
        System.out.println(costumerRepository.findAll());
//
        System.out.println(costumerRepository.findByEmail("www@mail.ru"));

        System.out.println(fileInfoRepository.findByStorageName("1234567.jpg"));

        System.out.println(costumerRepository.searchByEmail("ww"));

        System.out.println(fileInfoRepository.searchByOriginalFileName("T", 1L));





    }
}
