package ru.pcs.web.repositories;

import ru.pcs.web.models.AccountCostumer;
import ru.pcs.web.models.FileInfo;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.io.File;
import java.util.List;
import java.util.Optional;

public class FileInfoRepositoryJpa implements FileInfoRepository{

    private final EntityManager entityManager;

    public FileInfoRepositoryJpa(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void save(FileInfo file) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(file);
        transaction.commit();
    }

    @Override
    public Optional<FileInfo> findByStorageName(String storageFileName) {
        TypedQuery<FileInfo> query = entityManager.createQuery("select fileInfo from FileInfo fileInfo" +
                " where fileInfo.storageFileName = :storageFileName", FileInfo.class);
        query.setParameter("storageFileName", storageFileName);
        return Optional.of(query.getResultList().get(0));
    }

    @Override
    public List<FileInfo> searchByOriginalFileName(String originalFileName, Long userId) {
        TypedQuery<FileInfo> query = entityManager.createQuery("select file from FileInfo file " +
                " where file.originalFileName like :originalFileName and file.accountCostumer.id= :userId", FileInfo.class);
        query.setParameter("originalFileName", "%" + originalFileName + "%");
        query.setParameter("userId", userId);
        return query.getResultList();
    }
}
