package ru.pcs.web.repositories;

import ru.pcs.web.models.AccountCostumer;

import java.util.List;
import java.util.Optional;

public interface CostumerRepository extends CrudRepository<AccountCostumer>{
    void save(AccountCostumer costumer);
    List<AccountCostumer> findAll();
    Optional<AccountCostumer> findByEmail(String email);

    List<AccountCostumer> searchByEmail(String email);
}
