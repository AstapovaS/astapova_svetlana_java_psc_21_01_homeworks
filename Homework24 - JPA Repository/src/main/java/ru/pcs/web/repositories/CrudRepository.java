package ru.pcs.web.repositories;

public interface CrudRepository<T> {
    void save(T entity);
}
