package ru.pcs.web.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import ru.pcs.web.models.AccountCostumer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CostumerRepositoryHibernate implements CostumerRepository{
    @Override
    public void save(AccountCostumer costumer) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();


        session.save(costumer); // просто загружаем файл

        session.close();
        sessionFactory.close();
    }

    @Override
    public List<AccountCostumer> findAll() {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

        Query accountCostumerQuery = session.createQuery("FROM AccountCostumer ");
        List<AccountCostumer> accountCostumerList = accountCostumerQuery.list();

        session.close();
        sessionFactory.close();
        return accountCostumerList;
    }

    @Override
    public Optional<AccountCostumer> findByEmail(String email) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

        //логика
        Query accountQuery = session.createQuery("FROM AccountCostumer ");
        List<AccountCostumer> list = accountQuery.list();
        session.close();
        sessionFactory.close();
        for (AccountCostumer costumer : list) {
            if (costumer.getEmail().equals(email)) {
                return Optional.of(costumer);
            }
        }
        return Optional.empty();
    }

    @Override
    public List<AccountCostumer> searchByEmail(String email) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

        //логика
        Query costumerQuery = session.createQuery("FROM AccountCostumer ");

        List<AccountCostumer> allCostumersList = costumerQuery.list();

        List<AccountCostumer> costumerList = new ArrayList<>();

        for (AccountCostumer costumer : allCostumersList){
            if (costumer.getEmail().contains(email)
            ) {
                costumerList.add(costumer);
            }
        }
        session.close();
        sessionFactory.close();

        return costumerList;
    }
}
