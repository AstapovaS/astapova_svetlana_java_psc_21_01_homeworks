package ru.pcs.web.repositories;

import ru.pcs.web.models.AccountCostumer;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CostumerRepositoryJpa implements CostumerRepository{

    private final EntityManager entityManager;

    public CostumerRepositoryJpa(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void save(AccountCostumer costumer) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(costumer);
        transaction.commit();
    }

    @Override
    public List<AccountCostumer> findAll() {
//        Query query = entityManager.createQuery("from AccountCostumer ");
//        List<AccountCostumer> costumers = query.getResultList();
        System.out.println("УРА, РАБОТАЕТ");
        return entityManager.createQuery("from AccountCostumer", AccountCostumer.class).getResultList();
    }

    @Override
    public Optional<AccountCostumer> findByEmail(String email) {
        TypedQuery<AccountCostumer> query = entityManager.createQuery("select costumer from AccountCostumer costumer" +
                " where costumer.email = :email", AccountCostumer.class);
        query.setParameter("email", email);
        return Optional.of(query.getResultList().get(0));
    }

    @Override
    public List<AccountCostumer> searchByEmail(String email) {
        TypedQuery<AccountCostumer> query = entityManager.createQuery("select costumer from AccountCostumer costumer " +
                " where costumer.email like :email", AccountCostumer.class);
        query.setParameter("email", "%" + email + "%");
        return query.getResultList();
    }
}
