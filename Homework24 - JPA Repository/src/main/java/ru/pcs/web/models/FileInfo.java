package ru.pcs.web.models;

import lombok.*;

import javax.persistence.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = "accountCostumer")
@Entity
public class FileInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="original_name")
    private String originalFileName;
    @Column(name="storage_name")
    private String storageFileName;
    private Long size;
    @Column(name="mime_type")
    private String mimeType;
    private String description;
//    @Column(name="id_account_costumer")
//    private Long idAccountCostumer;

    @ManyToOne
    @JoinColumn(name="id_account_costumer")
    private AccountCostumer accountCostumer;
}

