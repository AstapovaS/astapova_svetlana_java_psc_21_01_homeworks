package ru.pcs.web.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class AccountCostumer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 250)
    private String name;
    private String email;
    private String password;


    @OneToMany(mappedBy = "accountCostumer")
    private List<FileInfo> listFiles;

}
