package ru.pcs.web.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.pcs.web.dto.LessonDto;
import ru.pcs.web.dto.LessonsResponse;
import ru.pcs.web.services.LessonService;

import java.time.LocalDate;

@RestController
@RequestMapping("/lessons")
@RequiredArgsConstructor
public class LessonController {
    private final LessonService lessonService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<LessonsResponse> getLessons(@RequestParam("page") int page, @RequestParam("size") int size){
        return ResponseEntity.ok()
                .headers(httpHeaders -> httpHeaders.add("dateTime", LocalDate.now().toString()))
                .body(LessonsResponse.builder().data(lessonService.getLesson(page, size)).build());
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public LessonDto addLesson(@RequestBody LessonDto lesson){
        return lessonService.addLesson(lesson);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{lesson-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public LessonDto updateLesson(@PathVariable("lesson-id") Long lessonId, @RequestBody LessonDto lesson) {
        return lessonService.updateLesson(lessonId, lesson);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{lesson-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteLesson(@PathVariable("lesson-id") Long lessonId) {
        lessonService.deleteLesson(lessonId);
    }
}
