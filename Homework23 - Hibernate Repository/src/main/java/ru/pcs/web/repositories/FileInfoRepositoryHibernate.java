package ru.pcs.web.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import ru.pcs.web.models.AccountCostumer;
import ru.pcs.web.models.FileInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FileInfoRepositoryHibernate implements FileInfoRepository{
    @Override
    public void save(FileInfo file) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();


        session.save(file); // просто загружаем файл

        session.close();
        sessionFactory.close();
    }

    @Override
    public Optional<FileInfo> findByStorageName(String storageFileName) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

        //логика: вытаскиваем список и фильтруем по уникальному имени
        Query fileInfoQuery = session.createQuery("FROM FileInfo ");
        List<FileInfo> list = fileInfoQuery.list();
        session.close();
        sessionFactory.close();
        for (FileInfo file : list) {
            if (file.getStorageFileName().equals(storageFileName)) {
                return Optional.of(file);
            }
        }
        return Optional.empty();
    }

    @Override
    public List<FileInfo> searchByOriginalFileName(String originalFileName, Long userId) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

        //логика: вытаскиваем список и фильтруем по имени и id
        Query fileInfoQuery = session.createQuery("FROM FileInfo");

        List<FileInfo> allFilesList = fileInfoQuery.list();

        List<FileInfo> fileInfoList = new ArrayList<>();

        for (FileInfo fileInfo : allFilesList){
            if (fileInfo.getOriginalFileName().contains(originalFileName)
                && fileInfo.getIdAccountCostumer()==userId
               ) {
                fileInfoList.add(fileInfo);
            }
        }
        session.close();
        sessionFactory.close();

        return fileInfoList;
    }
}
