package ru.pcs.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.web.models.AccountCostumer;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountDTO {
    private Long id;
    private String name;
    private String email;

    public static AccountDTO from (AccountCostumer accountCostumer) {
        return AccountDTO.builder()
                .id(accountCostumer.getId())
                .name(accountCostumer.getName())
                .email(accountCostumer.getEmail())
                .build();

    }

    public static List<AccountDTO> from (List<AccountCostumer> accounts){
        return accounts.stream().map(AccountDTO::from).collect(Collectors.toList());
    }

}
