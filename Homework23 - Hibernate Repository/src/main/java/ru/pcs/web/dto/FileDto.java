package ru.pcs.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.web.models.AccountCostumer;
import ru.pcs.web.models.FileInfo;

import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileDto {
    private Long size;
    private String fileName;
    private String mimeType;
    private InputStream fileStream;
    private String description;
    private String originalFileName;
    private Long idAccountDto;

    public static FileDto from(FileInfo fileInfo) {
        return FileDto.builder()
                .description(fileInfo.getDescription())
                .fileName(fileInfo.getStorageFileName())
                .size(fileInfo.getSize())
                .mimeType(fileInfo.getMimeType())
                .originalFileName(fileInfo.getOriginalFileName())
                .idAccountDto(fileInfo.getIdAccountCostumer())
                .build();
    }
    public static List<FileDto> from (List<FileInfo> files){
        return files.stream().map(FileDto::from).collect(Collectors.toList());
    }
}
