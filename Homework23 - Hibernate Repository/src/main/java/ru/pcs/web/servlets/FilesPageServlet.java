package ru.pcs.web.servlets;

import ru.pcs.web.repositories.CostumerRepository;
import ru.pcs.web.repositories.CostumerRepositoryJbdcImpl;
import ru.pcs.web.repositories.FileInfoRepository;
import ru.pcs.web.repositories.FileInfoRepositoryJdbcImpl;
import ru.pcs.web.services.FilesService;
import ru.pcs.web.services.FilesServiceImpl;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
@WebServlet("/files")
public class FilesPageServlet extends HttpServlet {
    private FilesService filesService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");
        FileInfoRepository fileInfoRepository = new FileInfoRepositoryJdbcImpl(dataSource);
        CostumerRepository costumerRepository = new CostumerRepositoryJbdcImpl(dataSource);
        this.filesService = new FilesServiceImpl(fileInfoRepository, costumerRepository);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/jsp/files.jsp").forward(request, response);
    }
}
