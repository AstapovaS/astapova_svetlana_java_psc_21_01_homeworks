package ru.pcs.web.servlets;

import ru.pcs.web.dto.AccountDTO;
import ru.pcs.web.filters.ColorFilter;
import ru.pcs.web.repositories.CostumerRepository;
import ru.pcs.web.repositories.CostumerRepositoryJbdcImpl;
import ru.pcs.web.services.ProfileService;
import ru.pcs.web.services.ProfileServiceImpl;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

@WebServlet("/profile")
public class ProfileServlet extends HttpServlet {
    private static final int COLOR_COOKIE_MAX_AGE = 60 * 60 * 24 * 365;
    private ProfileService profileService;

    @Override
    public void init(ServletConfig config) throws ServletException {

        ServletContext servletContext = config.getServletContext();
        DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");

        CostumerRepository repository = new CostumerRepositoryJbdcImpl(dataSource);

        this.profileService = new ProfileServiceImpl(repository);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pageColor = request.getParameter("color");

//        String name = request.getParameter("name");
//        String email = request.getParameter("email");
//        String id = request.getParameter("id");
//        String password = request.getParameter("password");

        if (pageColor!=null) {
            request.setAttribute("color", pageColor);
            Cookie cookie = new Cookie(ColorFilter.COLOR_COOKIE_NAME, pageColor);
            cookie.setMaxAge(COLOR_COOKIE_MAX_AGE);
            response.addCookie(cookie);
        }
        String email = SignInServlet.emailOfCurrentAccount;  //вытаскиваем email
        request.setAttribute("email", email);  // устанавливаем email для отображения на странице

        AccountDTO accountDTO = profileService.getAccountByEmail(email);

                //profileService.getAccountByEmail(email); //вытаскиваем пользователя из БД

        request.setAttribute("name", accountDTO.getName()); //устанавливаем данные для страницы
        request.setAttribute("id", accountDTO.getId());

        request.getRequestDispatcher("/jsp/profile.jsp").forward(request, response);
    }

}
