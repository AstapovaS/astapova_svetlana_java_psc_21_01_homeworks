package ru.pcs.web.servlets;

import ru.pcs.web.dto.AccountDTO;
import ru.pcs.web.dto.FileDto;
import ru.pcs.web.repositories.CostumerRepository;
import ru.pcs.web.repositories.CostumerRepositoryJbdcImpl;
import ru.pcs.web.repositories.FileInfoRepository;
import ru.pcs.web.repositories.FileInfoRepositoryJdbcImpl;
import ru.pcs.web.services.FilesService;
import ru.pcs.web.services.FilesServiceImpl;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.sql.DataSource;
import java.io.*;


@WebServlet("/filesUpload")
@MultipartConfig
public class FilesUploadServlet extends HttpServlet {

    private FilesService filesService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");
        FileInfoRepository fileInfoRepository = new FileInfoRepositoryJdbcImpl(dataSource);
        CostumerRepository costumerRepository = new CostumerRepositoryJbdcImpl(dataSource);
        this.filesService = new FilesServiceImpl(fileInfoRepository, costumerRepository);
        this.filesService.setStoragePath((String) servletContext.getAttribute("storagePath"));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/jsp/filesUpload.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader descriptionReader = new BufferedReader(
                new InputStreamReader(request.getPart("description").getInputStream()));

        Part filePart = request.getPart("file");
        // вытаскиваем текущего пользователя по email
        AccountDTO accountDTO = filesService.getCurrentAccountByEmail(SignInServlet.emailOfCurrentAccount);

        // создаем объект FileDTO,
        FileDto form = FileDto.builder()
                .description(descriptionReader.readLine())
                .fileName(filePart.getSubmittedFileName())
                .size(filePart.getSize())
                .mimeType(filePart.getContentType())
                .idAccountDto(accountDTO.getId()) // в объекте сохраняется ID текущего пользователя
                .fileStream(filePart.getInputStream())
                .build();

        filesService.upload(form);

        response.sendRedirect("/files");
    }
}
