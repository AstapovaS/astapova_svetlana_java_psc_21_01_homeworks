package ru.pcs.web.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.pcs.web.dto.FileDto;
import ru.pcs.web.repositories.CostumerRepository;
import ru.pcs.web.repositories.CostumerRepositoryJbdcImpl;
import ru.pcs.web.repositories.FileInfoRepository;
import ru.pcs.web.repositories.FileInfoRepositoryJdbcImpl;
import ru.pcs.web.services.FilesService;
import ru.pcs.web.services.FilesServiceImpl;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;

@WebServlet("/files/json")
public class FilesJsonServlet extends HttpServlet {
    private FilesService filesService;
    private ObjectMapper objectMapper;

    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");
        FileInfoRepository fileInfoRepository = new FileInfoRepositoryJdbcImpl(dataSource);
        CostumerRepository costumerRepository = new CostumerRepositoryJbdcImpl(dataSource);
        this.filesService = new FilesServiceImpl(fileInfoRepository, costumerRepository);
        this.objectMapper = new ObjectMapper();
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Long userId = filesService.getCurrentAccountByEmail(SignInServlet.emailOfCurrentAccount).getId();
        List<FileDto> files = filesService.searchByOriginalFileName(request.getParameter("originalFileName"), userId);
        response.setContentType("application/json");
        String json = objectMapper.writeValueAsString(files);
        response.getWriter().println(json);
    }

}
