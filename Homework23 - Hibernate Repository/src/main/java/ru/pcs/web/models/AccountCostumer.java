package ru.pcs.web.models;

import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountCostumer {
    private Long id;
    private String name;
    private String email;
    private String password;
//    private List<FileInfo> listFiles;

}
