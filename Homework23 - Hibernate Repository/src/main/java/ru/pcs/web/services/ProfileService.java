package ru.pcs.web.services;

import ru.pcs.web.dto.AccountDTO;

public interface ProfileService {
    AccountDTO getAccountByEmail(String email);
}
