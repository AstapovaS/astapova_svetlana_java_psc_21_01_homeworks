package ru.pcs.web.services;

import ru.pcs.web.dto.SignUpForm;
import ru.pcs.web.models.AccountCostumer;
import ru.pcs.web.repositories.CostumerRepository;

import java.util.Locale;

public class SignUpServiceImpl implements SignUpService{

    private final CostumerRepository costumerRepository;

    public SignUpServiceImpl(CostumerRepository costumerRepository) {
        this.costumerRepository = costumerRepository;
    }
    @Override
    public void signUp(SignUpForm form) {
        AccountCostumer accountCostumer = AccountCostumer.builder()
                .name(form.getName())
                .email(form.getEmail().toLowerCase(Locale.ROOT))
                .password(form.getPassword())
                .build();
        costumerRepository.save(accountCostumer);
    }
}
