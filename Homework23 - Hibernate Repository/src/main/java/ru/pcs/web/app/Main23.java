package ru.pcs.web.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.pcs.web.dto.SignUpForm;
import ru.pcs.web.models.AccountCostumer;
import ru.pcs.web.models.FileInfo;
import ru.pcs.web.repositories.*;
import ru.pcs.web.services.SignUpService;
import ru.pcs.web.services.SignUpServiceImpl;

import java.util.ArrayList;
import java.util.Properties;

public class Main23 {
    public static void main(String[] args) {

        FileInfoRepository repository = new FileInfoRepositoryHibernate();
        CostumerRepository costumerRepository = new CostumerRepositoryHibernate();
//        AccountCostumer costumer = AccountCostumer.builder()
//                .name("Svetlana")
//                .build();
//        costumerRepository.save(costumer);

        System.out.println(costumerRepository.searchByEmail("gmail"));

//        FileInfo file = FileInfo.builder()
//                .storageFileName("34434222dddss.jpg")
//                .mimeType("jpg")
//                .originalFileName("Test1.jpg")
//                .size(324434343434L)
//                .description("ererer")
//                .idAccountCostumer(3L)
//                .build();
//
//        repository.save(file);

        System.out.println(repository.findByStorageName("34434222dddss.jpg"));

        System.out.println(repository.searchByOriginalFileName("Test", 4L));



// Старый код:

//        Properties properties = new Properties();
//        try {
//            properties.load(ClassLoader.getSystemResourceAsStream("application.properties"));
//        } catch (Exception e) {
//            throw new IllegalArgumentException(e);
//        }
//
//        HikariConfig config = new HikariConfig();
//        config.setUsername(properties.getProperty("db.user"));
//        config.setPassword(properties.getProperty("db.password"));
//        config.setDriverClassName(properties.getProperty("db.driver-class-name"));
//        config.setJdbcUrl(properties.getProperty("db.url"));
//        config.setMaximumPoolSize(Integer.parseInt(properties.getProperty("db.hikari.max-pool-size")));
//
//        HikariDataSource dataSource = new HikariDataSource(config);
//        CostumerRepository repository = new CostumerRepositoryJbdcImpl(dataSource);
//
//        SignUpService service = new SignUpServiceImpl(repository);
//
//        SignUpForm form = SignUpForm.builder()
//                .email("morgan159@mail.ru")
//                .password("qwerty")
//                .name("Svetlana")
//                .build();
//
//        service.signUp(form);
//
//        System.out.println(repository.findAll());


    }
}
