<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Accounts</title>
</head>
<script>
    function addAccount(name) {
        let body = {
            "name": name,
            "email" : ""
        }

        let request = new XMLHttpRequest();

        request.open('POST', '/accounts/json', false);
        request.setRequestHeader('Content-Type', 'application/json');
        request.send(JSON.stringify(body));

        if (request.status !== 200) {
            alert("Ошибка!")
        } else {
            let html =
                '<tr>' +
                '<th>ID</th>' +
                '<th>Name</th>' +
                '</tr>';

            let json = JSON.parse(request.response);

            for (let i = 0; i < json.length; i++) {
                html += '<tr>'
                html += '<td>' + json[i]['id'] + '</td>'
                html += '<td>' + json[i]['name'] + '</td>'
                html += '</tr>'
            }

            document.getElementById('accounts_table').innerHTML = html;
        }

    }
</script>
<body>
<h1 style="color: ${color}">Accounts size - ${accounts.size()}</h1>
<table id="accounts_table">
    <tr>
        <th>ID</th>
        <th>Name</th>
    </tr>
    <c:forEach items="${accounts}" var="account">
        <tr>
            <td>${account.id}</td>
            <td>${account.name}</td>
        </tr>
    </c:forEach>
</table>
<div>
    <input id="name" placeholder="Enter Name">
    <button onclick="addAccount(
        document.getElementById('name').value)">
        Add
    </button>
</div>
</body>
</html>

