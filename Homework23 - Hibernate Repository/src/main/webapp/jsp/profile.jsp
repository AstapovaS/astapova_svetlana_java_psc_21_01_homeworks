<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Profile</title>
</head>
<body>
<h1 style="color: ${color}">Profile</h1>
<b>Your name: ${name}</b>
<br>
<b>Your email: ${email}</b>
<br>
<b>Your password: нет прав доступа!!!</b>
<br>
<b>Your id: ${id}</b>
<br>
<a href="accounts">Все пользователи</a>
<br>
<a href="filesUpload">Загрузить файл</a>
<a href="files">Поиск файлов пользователя</a>
<br>
<c:set var="Solution" value="Выход"/>
<button><a href="<%=request.getContextPath()%>/LogOut">${Solution}</a></button>
</body>
</html>
