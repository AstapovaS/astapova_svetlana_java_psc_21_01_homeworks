package ru.pcs.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AddGoodForm {

    @Size(min = 5, max = 40)
    @NotBlank
    private String goodName;

    @Size(min = 100, max = 500)
    @NotBlank
    private String description;

    @NotNull
    @PositiveOrZero
    private Double cost;

    @NotNull
    @PositiveOrZero
    private Integer count;
}

