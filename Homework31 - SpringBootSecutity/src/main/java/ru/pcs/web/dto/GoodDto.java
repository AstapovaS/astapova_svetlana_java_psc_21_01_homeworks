package ru.pcs.web.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.web.models.Good;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GoodDto {
    private Long id;
    private String name;
    private String description;
    private Double cost;
    private Integer count;

    public static GoodDto from(Good good) {
        return GoodDto.builder()
                .id(good.getId())
                .name(good.getGoodName())
                .description(good.getDescription())
                .cost(good.getCost())
                .count(good.getCount())
                .build();
    }

    public static List<GoodDto> from(List<Good> goods) {
        return goods.stream().map(GoodDto::from).collect(Collectors.toList());
    }
}
