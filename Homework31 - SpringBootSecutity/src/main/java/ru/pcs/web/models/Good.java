package ru.pcs.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Good {

    public enum State {
        CONFIRMED, DELETED
    };
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private Good.State state;

    @Column(name = "good_name")
    private String goodName;

    @Column(name = "description")
    private String description;

    private Double cost;
    private Integer count;
}
