package ru.pcs.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.web.dto.GoodDto;
import ru.pcs.web.models.Good;
import ru.pcs.web.repositories.GoodsRepository;

import static ru.pcs.web.dto.GoodDto.from;


import java.util.List;

@RequiredArgsConstructor
@Service
public class CatalogServiceImpl implements CatalogService{
    private final GoodsRepository goodsRepository;
    @Override
    public List<GoodDto> getAllGoods() {
        return from(goodsRepository.findAll());
    }

    @Override
    public void deleteGood(Long goodId) {
        Good good = goodsRepository.getById(goodId);
        good.setState(Good.State.DELETED);
        goodsRepository.save(good);
    }
}
