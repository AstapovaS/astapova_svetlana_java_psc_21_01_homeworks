package ru.pcs.web.services;

import ru.pcs.web.dto.AddGoodForm;

public interface AddGoodsService {
    void addGood(AddGoodForm form);
}

