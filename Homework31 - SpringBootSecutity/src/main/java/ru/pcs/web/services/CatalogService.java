package ru.pcs.web.services;

import ru.pcs.web.dto.GoodDto;

import java.util.List;

public interface CatalogService {
    List<GoodDto> getAllGoods();

    void deleteGood(Long goodId);
}
