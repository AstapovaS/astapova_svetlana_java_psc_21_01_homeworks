package ru.pcs.web.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.pcs.web.services.AccountsService;
import ru.pcs.web.services.CatalogService;

@RequiredArgsConstructor
@Controller
@RequestMapping("/catalog")
public class CatalogController {

    private final CatalogService catalogService;

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping
    public String getCatalogPage(Model model) {
        model.addAttribute("catalog", catalogService.getAllGoods());
        return "catalog";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/{good-id}/delete")
    public String deleteAccount(@PathVariable("good-id") Long goodId) {
        catalogService.deleteGood(goodId);
        return "redirect:/catalog";
    }
}
