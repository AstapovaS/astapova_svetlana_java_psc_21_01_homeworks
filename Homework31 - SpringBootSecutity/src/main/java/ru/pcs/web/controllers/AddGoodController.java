package ru.pcs.web.controllers;


import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.pcs.web.dto.AddGoodForm;
import ru.pcs.web.services.AddGoodsService;

import javax.validation.Valid;

@RequiredArgsConstructor
@Controller
@RequestMapping("/addGood")
public class AddGoodController {
    private final AddGoodsService addGoodsService;

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping
    public String getAddGoodPage(Model model) {
        model.addAttribute("addGoodForm", new AddGoodForm());
        return "addGood";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping
    public String addGood(@Valid AddGoodForm form, BindingResult result, Model model){
        if (result.hasErrors()) {
            model.addAttribute("addGoodForm", form);
            return "addGood";
        }
        addGoodsService.addGood(form);
        return "redirect:/catalog";
    }
}
