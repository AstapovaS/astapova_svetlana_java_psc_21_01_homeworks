public class Sword extends Weapon{

    private double weight;
    private boolean underOneArm;


    private Sword(SwordWeaponBuilder swordWeaponBuilder) {
        this.damage = swordWeaponBuilder.damage;
        this.strength = swordWeaponBuilder.strength;
        this.weight = swordWeaponBuilder.weight;
        this.underOneArm = swordWeaponBuilder.underOneArm;
    }

    public Sword() {

    }

    public Sword (Sword target) {
        super(target);
        if (target != null) {
            this.underOneArm = target.underOneArm;
            this.weight = target.weight;
        }
    }

    public static class SwordWeaponBuilder extends WeaponBuilder{
        private int damage;
        private int strength;
        private double weight;
        private boolean underOneArm;


        public SwordWeaponBuilder damage(int damage) {
            this.damage = damage;
            return this;
        }

        public SwordWeaponBuilder strength(int strength) {
            this.strength = strength;
            return this;
        }

        public SwordWeaponBuilder weight (double weight) {
            this.weight = weight;
            return this;
        }

        public SwordWeaponBuilder underOneArm(boolean underOneArm) {
            this.underOneArm = underOneArm;
            return this;
        }

        public Sword build() {
            return new Sword(this);
        }
    }

    public static SwordWeaponBuilder builder() {
        return new SwordWeaponBuilder();
    }


    public double getWeight() {
        return weight;
    }

    public boolean isUnderOneArm() {
        return underOneArm;
    }

    @Override
    public Weapon clone() {
        return new Sword(this);
    }

    @Override
    public String toString() {
        return "Sword{" +
                "damage=" + damage +
                ", strength=" + strength +
                ", weight=" + weight +
                ", underOneArm=" + underOneArm +
                '}';
    }
}
