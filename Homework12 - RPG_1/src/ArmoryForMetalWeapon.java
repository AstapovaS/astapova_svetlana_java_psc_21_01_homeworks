public class ArmoryForMetalWeapon implements Armory{

    private static class MetalSword extends Sword {

    }

    @Override
    public Sword createSword() {
        return Sword.builder().build();
    }

    private static class MetalBow extends Bow {

    }

    @Override
    public Bow createBow() {
        return Bow.builder().build();
    }

    private static class MetalKnife extends Knife {

    }

    @Override
    public Knife createKnife() {
        return Knife.builder().build();
    }
}
