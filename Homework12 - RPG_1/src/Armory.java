public interface Armory {
    Sword createSword();
    Bow createBow();
    Knife createKnife();
}
