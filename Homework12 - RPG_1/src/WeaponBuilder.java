public class WeaponBuilder{
    private int damage;
    private int strength;

    public WeaponBuilder damage(int damage) {
        this.damage = damage;
        return this;
    }

    public WeaponBuilder strength(int strength) {
        this.strength = strength;
        return this;
    }
}
