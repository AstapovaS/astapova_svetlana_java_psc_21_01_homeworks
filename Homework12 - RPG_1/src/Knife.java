public class Knife extends Weapon {
    private Knife(KnifeBuilder knifeBuilder) {
        this.damage = knifeBuilder.damage;
        this.strength = knifeBuilder.strength;
    }

    public Knife() {

    }

    public Knife(Knife target) {
        super(target);
    }

    public static class KnifeBuilder extends WeaponBuilder{
        private int damage;
        private int strength;


        public KnifeBuilder damage(int damage) {
            this.damage = damage;
            return this;
        }

        public KnifeBuilder strength(int strength) {
            this.strength = strength;
            return this;
        }


        public Knife build() {
            return new Knife(this);
        }
    }

    public static KnifeBuilder builder() {
        return new KnifeBuilder();
    }

    @Override
    public Weapon clone() {
        return new Knife(this);
    }

    @Override
    public String toString() {
        return "Knife{" +
                "strength=" + strength +
                ", damage=" + damage +
                '}';
    }
} // никаких дополнительных характеристик
