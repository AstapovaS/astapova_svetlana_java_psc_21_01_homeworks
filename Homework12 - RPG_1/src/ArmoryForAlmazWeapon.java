public class ArmoryForAlmazWeapon implements Armory{
    private static class AlmazSword extends Sword {

    }

    @Override
    public Sword createSword() {
        return new AlmazSword();
    }

    private static class AlmazBow extends Bow {
    }

    @Override
    public Bow createBow() {
        return new AlmazBow();
    }

    private static class AlmazKnife extends Knife {

    }

    @Override
    public Knife createKnife() {
        return new AlmazKnife();
    }
}
