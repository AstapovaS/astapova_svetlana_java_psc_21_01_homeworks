abstract class Weapon {
    int strength;
    int damage;

    public Weapon(){}

    public Weapon(Weapon target) {
        this.damage = target.damage;
        this.strength = target.strength;
    }

    public int getDamage() {
        return damage;
    }

    public int getStrength() {
        return strength;
    }

    public abstract Weapon clone();
}
