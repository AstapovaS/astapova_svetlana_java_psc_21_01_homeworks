public class Main12 {
    public static void main(String[] args) {
        Armory armory = new ArmoryForWoodWeapon();
        Sword sword = armory.createSword();
        System.out.println(sword);

        Armory armory1 = new ArmoryForMetalWeapon();
        Bow bow = Bow.builder()
                .damage(10)
                .range(13)
                .speedReload(2)
                .build();
        System.out.println(bow);


    }
}

