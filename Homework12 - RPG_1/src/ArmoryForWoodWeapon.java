public class ArmoryForWoodWeapon implements Armory{
    private static class WoodSword extends Sword {

    }

    @Override
    public Sword createSword() {
        return new WoodSword();
    }

    private static class WoodBow extends Bow {

    }

    @Override
    public Bow createBow() {
        return new WoodBow();
    }

    private static class WoodKnife extends Knife{

    }

    @Override
    public Knife createKnife() {
        return new WoodKnife();
    }


}
