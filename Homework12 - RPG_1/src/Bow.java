public class Bow extends Weapon{

    private int speedReload;
    private int range;


    private Bow(BowWeaponBuilder bowWeaponBuilder) {
        this.damage = bowWeaponBuilder.damage;
        this.strength = bowWeaponBuilder.strength;
        this.speedReload = bowWeaponBuilder.speedReload;
        this.range = bowWeaponBuilder.range;
    }

    public Bow() {

    }

    public Bow (Bow target) {
        super(target);
        if (target != null) {
            this.range = target.range;
            this.speedReload = target.speedReload;
        }
    }


    public static class BowWeaponBuilder extends WeaponBuilder{
        private int damage;
        private int strength;
        private int speedReload;
        private int range;


        public BowWeaponBuilder damage(int damage) {
            this.damage = damage;
            return this;
        }

        public BowWeaponBuilder strength(int strength) {
            this.strength = strength;
            return this;
        }

        public BowWeaponBuilder speedReload (int speedReload) {
            this.speedReload = speedReload;
            return this;
        }

        public BowWeaponBuilder range(int range) {
            this.range = range;
            return this;
        }

        public Bow build() {
            return new Bow(this);
        }
    }

    public static BowWeaponBuilder builder() {
        return new BowWeaponBuilder();
    }


    public int getSpeedReload() {
        return speedReload;
    }

    public int getRange() {
        return range;
    }


    @Override
    public Weapon clone(){
        return new Bow(this);
    }

    @Override
    public String toString() {
        return "Bow{" +
                "speedReload=" + speedReload +
                ", range=" + range +
                ", strength=" + strength +
                ", damage=" + damage +
                '}';
    }
}
