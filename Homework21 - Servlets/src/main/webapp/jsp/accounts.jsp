<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<%@ page import="ru.pcs.web.dto.AccountDTO" %>--%>
<%--<%@ page import="java.util.List" %>&lt;%&ndash;--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Account</title>
</head>
<body>
<h1 style="color: ${color}">Accounts  size = ${accounts.size()}</h1>
<table>
    <tr>
        <th>ID</th>
        <th>Name</th>
    </tr>

<c:forEach items="${accounts}" var="account">
    <tr>
        <td>${account.id}</td>
        <td>${account.name}</td>
    </tr>
</c:forEach>
</table>
</body>
</html>
