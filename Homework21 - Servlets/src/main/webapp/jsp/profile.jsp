<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Profile</title>
</head>
<body>
<h1 style="color: ${color}">Profile</h1>
<h2>Your name: ${name}</h2>
<h2>Your email: ${email}</h2>
<h2>Your password: нет прав доступа</h2>
<h2>Your id: ${id}</h2>
<c:set var="Solution" value="Выход"/>
<button><a href="<%=request.getContextPath()%>/LogOut">${Solution}</a></button>
</body>
</html>
