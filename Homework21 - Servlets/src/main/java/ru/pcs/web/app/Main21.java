package ru.pcs.web.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.pcs.web.dto.SignUpForm;
import ru.pcs.web.repositories.CostumerRepository;
import ru.pcs.web.repositories.CostumerRepositoryImpl;
import ru.pcs.web.services.SignUpService;
import ru.pcs.web.services.SignUpServiceImpl;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Main21 {
    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(ClassLoader.getSystemResourceAsStream("application.properties"));
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }

        HikariConfig config = new HikariConfig();
        config.setUsername(properties.getProperty("db.user"));
        config.setPassword(properties.getProperty("db.password"));
        config.setDriverClassName(properties.getProperty("db.driver-class-name"));
        config.setJdbcUrl(properties.getProperty("db.url"));
        config.setMaximumPoolSize(Integer.parseInt(properties.getProperty("db.hikari.max-pool-size")));

        HikariDataSource dataSource = new HikariDataSource(config);
        CostumerRepository repository = new CostumerRepositoryImpl(dataSource);

        SignUpService service = new SignUpServiceImpl(repository);

        SignUpForm form = SignUpForm.builder()
                .email("morgan159@mail.ru")
                .password("qwerty")
                .name("Svetlana")
                .build();

        service.signUp(form);

        System.out.println(repository.findAll());

    }
}
