package ru.pcs.web.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.pcs.web.dto.*;
import ru.pcs.web.services.CourseService;

import java.time.LocalDate;

@RestController
@RequestMapping("/courses")
@RequiredArgsConstructor
public class CourseController {
    private final CourseService courseService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<CoursesResponse> getCourses(@RequestParam("page") int page, @RequestParam("size") int size){
        return ResponseEntity.ok()
                .headers(httpHeaders -> httpHeaders.add("dateTime", LocalDate.now().toString()))
                .body(CoursesResponse.builder().data(courseService.getCourse(page, size)).build());
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public CourseDto addCourse(@RequestBody CourseDto course){
        return courseService.addCourse(course);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{course-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public CourseDto updateCourse(@PathVariable("course-id") Long courseId, @RequestBody CourseDto course) {
        return courseService.updateCourse(courseId, course);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{course-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteCourse(@PathVariable("course-id") Long courseId) {
        courseService.deleteCourse(courseId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{course-id}/lessons")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public LessonsResponse addLessonToCourse(@PathVariable("course-id") Long courseId, @RequestBody LessonDto lesson){
        return LessonsResponse.builder()
                .data(courseService.addLessonToCourse(courseId, lesson))
                .build();
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{course-id}/lessons")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteLessonOfCourse(@PathVariable("course-id") Long courseId, @RequestBody LessonDto lesson) {
        courseService.deleteLessonOfCourse(courseId, lesson);
    }
}
