package ru.pcs.web.aspects;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ru.pcs.web.aspects.repo.CallsMethodService;
import ru.pcs.web.aspects.repo.CallsMethodsRepository;
import ru.pcs.web.aspects.repo.Model;
import ru.pcs.web.dto.WrapperResponse;

import java.lang.reflect.Method;


@Component
@Aspect
@Slf4j
public class CallCountsAspect {
    @Autowired
    private CallsMethodService callsMethodService;

    private String name;

    @Around("execution(* ru.pcs.web.controllers.*.*(..))")
    public ResponseEntity<WrapperResponse> countCallMethods(ProceedingJoinPoint joinPoint) throws Throwable {
        this.name = MethodSignature.class.cast(joinPoint.getSignature()).getMethod().getName();
        Object response = joinPoint.proceed();
        WrapperResponse resultResponse = WrapperResponse.builder()
                .response(response)
                .build();
        return ResponseEntity.ok(resultResponse);

    }

    @Before("execution(* ru.pcs.web.controllers.*.*(..))")
    public void handler() {
        this.toCount();
    }

    public void toCount() {
        if (callsMethodService.findCallsMethodByName(this.name).isPresent()) {
            callsMethodService.updateCallsMethod(
                    callsMethodService.findCallsMethodByName(this.name).get().getId(),
                    callsMethodService.findCallsMethodByName(this.name).get());
        } else {
            Model model = Model.builder().name(this.name).count(1).build();
            callsMethodService.addCallsMethod(model);
        }
    }


}


