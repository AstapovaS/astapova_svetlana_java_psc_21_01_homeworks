package ru.pcs.web.aspects.repo;


import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CallsMethodsRepository extends JpaRepository<Model, Long> {
    Optional<Model> findByName(String name);
}


