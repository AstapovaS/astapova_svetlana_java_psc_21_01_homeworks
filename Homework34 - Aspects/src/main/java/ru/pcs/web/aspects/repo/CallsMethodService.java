package ru.pcs.web.aspects.repo;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CallsMethodService {
    @Autowired
    private final CallsMethodsRepository callsMethodsRepository;
    public Optional<Model> findCallsMethodByName(String name) {
        if (callsMethodsRepository.findByName(name).isPresent()) {
            return Optional.of(callsMethodsRepository.findByName(name).get());
        } else {
            return Optional.empty();
        }
    }

    public Model addCallsMethod(Model model) {
        callsMethodsRepository.save(model);
        return model;
    }

    public void updateCallsMethod(Long modelId, Model model) {
        Model existedCallsMethod = callsMethodsRepository.getById(modelId);
        existedCallsMethod.setCount(model.getCount() + 1);
        callsMethodsRepository.save(existedCallsMethod);
    }
}
