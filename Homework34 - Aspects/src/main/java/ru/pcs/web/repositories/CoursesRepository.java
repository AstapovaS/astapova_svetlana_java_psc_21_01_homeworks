package ru.pcs.web.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;
import ru.pcs.web.models.Course;

public interface CoursesRepository extends JpaRepository<Course, Long> {
    Page<Course> findAllByIsDeletedIsNull(Pageable pageable);

}
