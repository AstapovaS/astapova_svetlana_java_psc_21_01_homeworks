package ru.pcs.web.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.pcs.web.dto.CourseDto;
import ru.pcs.web.dto.CoursesResponse;
import ru.pcs.web.dto.StudentDto;
import ru.pcs.web.dto.StudentsResponse;
import ru.pcs.web.services.StudentsService;

import java.time.LocalDate;


@RestController // вешает респонбади на все методы/ответы - так будет возвращаться тело ответа
@RequestMapping("/api/students") //адрес страницы, которую контролирует класс
@RequiredArgsConstructor
public class StudentsController {

    private final StudentsService studentsService;

    @GetMapping("/{student-id}")
    public ResponseEntity<StudentDto> getStudent(@PathVariable("student-id") Long studentId) {
        return ResponseEntity.ok(studentsService.getStudent(studentId));
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<StudentsResponse> getStudents(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ResponseEntity.ok()
                .headers(httpHeaders -> httpHeaders.add("dateTime", LocalDate.now().toString()))
                .body(StudentsResponse.builder().data(studentsService.getStudents(page, size)).build());
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<StudentDto> addStudent(@RequestBody StudentDto student) {
        return ResponseEntity.ok(studentsService.addStudent(student));
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{student-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<StudentDto> updateStudent(@PathVariable("student-id") Long studentId, @RequestBody StudentDto student) {
        return ResponseEntity.ok(studentsService.updateStudent(studentId, student));
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{student-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteStudent(@PathVariable("student-id") Long studentId) {
        studentsService.deleteStudent(studentId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{student-id}/courses")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CoursesResponse> addCourseToStudent(@PathVariable("student-id") Long studentId,
                                                    @RequestBody CourseDto course) {
        return ResponseEntity.ok(CoursesResponse.builder()
                .data(studentsService.addCourseToStudent(studentId, course))
                .build());
    }
}
