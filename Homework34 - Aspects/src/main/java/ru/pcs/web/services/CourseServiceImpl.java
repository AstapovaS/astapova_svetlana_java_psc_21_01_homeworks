package ru.pcs.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import ru.pcs.web.dto.CourseDto;
import ru.pcs.web.dto.LessonDto;
import ru.pcs.web.models.Course;
import ru.pcs.web.models.Lesson;
import ru.pcs.web.repositories.CoursesRepository;
import ru.pcs.web.repositories.LessonsRepository;

import java.util.List;

import static ru.pcs.web.dto.LessonDto.from;

@Service
@RequiredArgsConstructor
public class CourseServiceImpl implements CourseService{
    private final CoursesRepository coursesRepository;
    private final LessonsRepository lessonsRepository;

    @Override
    public List<CourseDto> getCourse(int page, int size) {
        PageRequest request = PageRequest.of(page, size, Sort.by("id"));
        Page<Course> result = coursesRepository.findAllByIsDeletedIsNull(request);
        return CourseDto.from(result.getContent());
    }

    @Override
    public CourseDto addCourse(CourseDto course) {
        Course newCourse = Course.builder()
                .id(course.getId())
                .title(course.getTitle())
                .build();

        coursesRepository.save(newCourse);

        return CourseDto.from(newCourse);
    }

    @Override
    public CourseDto updateCourse(Long courseId, CourseDto course) {

        Course existedCourse = coursesRepository.getById(courseId);
        existedCourse.setTitle(course.getTitle());
        coursesRepository.save(existedCourse);
        return CourseDto.from(existedCourse);
    }

    @Override
    public void deleteCourse(Long courseId) {
        Course course = coursesRepository.getById(courseId);
        course.setIsDeleted(true);
        coursesRepository.save(course);
    }

    @Override
    public List<LessonDto> addLessonToCourse(Long courseId, LessonDto lesson) {
        Course course = coursesRepository.getById(courseId);
        Lesson existedLesson = lessonsRepository.getById(lesson.getId());
        course.getLessons().add(existedLesson);
        coursesRepository.save(course);
        return from(course.getLessons());
    }

    @Override
    public void deleteLessonOfCourse(Long courseId, LessonDto lesson) {
        Course course = coursesRepository.getById(courseId);
        Lesson existedLesson = lessonsRepository.getById(lesson.getId());
        course.getLessons().remove(existedLesson);
        coursesRepository.save(course);
    }
}
