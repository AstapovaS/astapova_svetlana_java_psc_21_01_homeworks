package ru.pcs.web.services;

import ru.pcs.web.dto.LessonDto;

import java.util.List;

public interface LessonService {
    List<LessonDto> getLesson(int page, int size);

    LessonDto addLesson(LessonDto lesson);

    LessonDto updateLesson(Long lessonId, LessonDto lesson);

    void deleteLesson(Long lessonId);


    LessonDto getLesson(Long lessonId);
}
