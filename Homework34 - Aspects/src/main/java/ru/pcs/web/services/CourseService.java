package ru.pcs.web.services;

import ru.pcs.web.dto.CourseDto;
import ru.pcs.web.dto.LessonDto;

import java.util.List;

public interface CourseService {
    List<CourseDto> getCourse(int page, int size);

    CourseDto addCourse(CourseDto course);

    CourseDto updateCourse(Long courseId, CourseDto course);

    void deleteCourse(Long courseId);

    List<LessonDto> addLessonToCourse(Long courseId, LessonDto lesson);

    void deleteLessonOfCourse(Long courseId, LessonDto lesson);
}
