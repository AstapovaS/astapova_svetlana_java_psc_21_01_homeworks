package ru.pcs.web.models;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountCostumer {
    private Long id;
    private String name;
    private String email;
    private String password;

}
