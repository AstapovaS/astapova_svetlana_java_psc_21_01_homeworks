package ru.pcs.web.repositories;

import ru.pcs.web.models.FileInfo;

import java.util.List;
import java.util.Optional;


public interface FileInfoRepository {
    void save(FileInfo file);

    Optional<FileInfo> findByStorageName(String storageFileName);

    List<FileInfo> searchByOriginalFileName(String originalFileName, Long userId);

}
