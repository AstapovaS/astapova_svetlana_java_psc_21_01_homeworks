package ru.pcs.web.repositories;


import ru.pcs.web.models.FileInfo;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;


public class FileInfoRepositoryJdbcImpl implements FileInfoRepository {

    //language=SQL
    private static final String SQL_INSERT = "insert into " +
            "fileinfo(originalfilename, storagefilename, size, mimetype, description, current_user_id) " +
            "values(?, ?, ?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_SELECT_BY_STORAGE_NAME = "select * from fileinfo where storagefilename = ?";

    //language=SQL
    private static final String SQL_LIKE_BY_ORIGINAL_NAME = "select * from fileinfo where originalfilename like ? and current_user_id = ?";

    private final DataSource dataSource;

    public FileInfoRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static final Function<ResultSet, FileInfo> fileInfoMapper = row -> {
        try {
            return FileInfo.builder()
                    .id(row.getLong("id"))
                    .storageFileName(row.getString("storagefilename"))
                    .description(row.getString("description"))
                    .originalFileName(row.getString("originalfilename"))
                    .mimeType(row.getString("mimetype"))
                    .size(row.getLong("size"))
                    .idAccountCostumer(row.getLong("current_user_id"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };

    @Override
    public void save(FileInfo file) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, file.getOriginalFileName());
            statement.setString(2, file.getStorageFileName());
            statement.setLong(3, file.getSize());
            statement.setString(4, file.getMimeType());
            statement.setString(5, file.getDescription());
            statement.setLong(6, file.getIdAccountCostumer());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert FileInfo");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                file.setId(generatedKeys.getLong("id"));
            } else {
                throw new SQLException("Can't get id");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<FileInfo> findByStorageName(String storageFileName) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_STORAGE_NAME)) {
            statement.setString(1, storageFileName);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(fileInfoMapper.apply(resultSet));
                }

                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<FileInfo> searchByOriginalFileName(String originalFileName, Long userId) {
        List<FileInfo> files = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_LIKE_BY_ORIGINAL_NAME)){
            statement.setString(1, "%" + originalFileName + "%");
            statement.setLong(2, userId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    files.add(fileInfoMapper.apply(resultSet));
                }
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return files;
    }

}
