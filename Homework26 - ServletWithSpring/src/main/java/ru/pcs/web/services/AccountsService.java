package ru.pcs.web.services;

import ru.pcs.web.dto.AccountDTO;

import java.util.List;

public interface AccountsService {
    List<AccountDTO> getAll();
    List<AccountDTO> searchByEmail(String email);
}

