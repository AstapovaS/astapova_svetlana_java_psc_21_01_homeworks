package ru.pcs.web.services;

import ru.pcs.web.dto.AccountDTO;
import ru.pcs.web.repositories.CostumerRepository;

import java.util.List;

import static ru.pcs.web.dto.AccountDTO.from;

public class AccountsServiceImpl implements AccountsService{
    private final CostumerRepository repository;

    public AccountsServiceImpl(CostumerRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<AccountDTO> getAll() {
        return from(repository.findAll());
    }

    @Override
    public List<AccountDTO> searchByEmail(String email) {
        return from(repository.searchByEmail(email));
    }


}
