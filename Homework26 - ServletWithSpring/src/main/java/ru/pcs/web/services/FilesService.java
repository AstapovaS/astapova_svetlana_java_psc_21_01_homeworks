package ru.pcs.web.services;

import ru.pcs.web.dto.AccountDTO;
import ru.pcs.web.dto.FileDto;

import java.io.OutputStream;
import java.util.List;


public interface FilesService {
    void upload(FileDto form);

    void setStoragePath(String path);

    FileDto getFile(String storageFileName);

    void writeFile(FileDto file, OutputStream outputStream);

    AccountDTO getCurrentAccountByEmail(String email);

    List<FileDto> searchByOriginalFileName(String originalFileName, Long userId);

}
