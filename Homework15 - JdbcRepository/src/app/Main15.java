package app;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.pcs.models.Product;
import ru.pcs.repositories.ProductsRepository;
import ru.pcs.repositories.ProductsRepositoryJdbc;


import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main15 {
    public static final String ORG_POSTGRESQL_DRIVER = "org.postgresql.Driver";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "27091986";
    private static final int MAX_CLIENTS_COUNT = 10;
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/test";

    public static void main(String[] args) {
        HikariConfig config = new HikariConfig();

        config.setUsername(DB_USER);
        config.setPassword(DB_PASSWORD);
        config.setDriverClassName(ORG_POSTGRESQL_DRIVER);
        config.setJdbcUrl(DB_URL);
        config.setMaximumPoolSize(20);
        HikariDataSource dataSource = new HikariDataSource(config);

        ProductsRepository productsRepository = new ProductsRepositoryJdbc(dataSource);

        ExecutorService service = Executors.newFixedThreadPool(MAX_CLIENTS_COUNT);

        for (int clientNumber = 0; clientNumber < MAX_CLIENTS_COUNT; clientNumber++) {
            service.submit(() -> {
                for (int i = 0; i < 20; i++) {
                    try {
                        System.out.println(productsRepository.findAll(i, 100));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        service.shutdown();

        // Добавление нового товара

        Product product = new Product("Кустарники", "Черная смородина", 3000.0, 4, 10);
        productsRepository.save(product);
        System.out.println(product);

        // Найти все (пагинация)

        System.out.println(productsRepository.findAll(1, 5));

        // Поиск по ID

        System.out.println(productsRepository.findById(5L));

        // Обновление данных о товаре
        Optional<Product> productOptional = productsRepository.findById(2L);
        productOptional.ifPresent(product1 -> {
                product1.setCategory("Хвойные");
                product1.setDiscount(0);
                product1.setStock(1);
                product1.setPrice(4000.0);
                product1.setName("Елка");
                productsRepository.update(product1);
                System.out.println(product1);
            });



        System.out.println("Товар " + productsRepository.findById(37L).get().getId() + " будет удален");

        // Удаление товара по ID
        productsRepository.deleteById(37L);

        System.out.println("Товар " + product.getId() + " будет удален");

        // Удаление товара по объекту (объект объявлен выше)
        productsRepository.delete(product);
    }
}

